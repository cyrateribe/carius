module.exports = {
    db: {
        user: 'postgres',
        database: 'carius_db',
        password: "16051104",
        host: "localhost",
        port: 5432,
        max: parseInt(process.env.DB_MAX_CLIENT), // max number of clients in the pool
        idleTimeoutMillis: parseInt(process.env.DB_IDLE_TIMEOUT), // how long a client is allowed to remain idle before being closed
    },
    redis: {
        host: "127.0.0.1",
        port: parseInt(6379),
        passord: ''
    },
    cache: {
        resetPasswordExpiration: parseInt(process.env.CACHE_RESET_PASSWORD_EXPIRATION)
    },
    server: {
        port: parseInt(process.env.SERVER_PORT || 3000),
        frontEndDomain: process.env.FRONT_END_DOMAIN
    },
    accessTokenJwt: {
        secret: "secretToken",
        options: {
            expiresIn: process.env.ACCESS_TOKEN_JWT_EXPIRATION
        }
    },
    aws: {
        bucket: process.env.AWS_BUCKET
    },
    mailgun: {
        apiKey: process.env.MAILGUN_API_KEY,
        domain: process.env.MAILGUN_DOMAIN
    },
    email: {
        from: process.env.EMAIL_FROM
    }
};
