var passport = require('passport');

module.exports = function (app) {

    app.get('/', (req, res) => {
        res.send('Pong');
    });

    //User
    app.post('/signup', require('./user/controller/user.signup'));
    app.post('/user/activate/:token', require('./user/controller/user.activate'));
    app.post('/login', require('./user/controller/user.login'));

    //TODO: updated_at current date
    app.patch('/user', passport.authenticate('authenticated', {session: false}), require('./user/controller/user.update'));

    //Car
    app.post('/car/manufacturer/create', passport.authenticate('authenticated', {session: false}), require('./car/controller/manufacturer.create'));
    app.post('/car/model/create', passport.authenticate('authenticated', {session: false}), require('./car/controller/model.create'));
    app.post('/car/body/create', passport.authenticate('authenticated', {session: false}), require('./car/controller/body.create'));


    //Vehicle
    app.post('/car/create', passport.authenticate('authenticated', {session: false}), require('./vehicle/controller/vehicle.create'));

    //Services Admin
    app.post('/service/basicservices/create', passport.authenticate('authenticated', {session: false}), require('./service/controller/basic.service.create'));

    //Services
    app.post('/service/create', passport.authenticate('authenticated', {session: false}), require('./service/controller/car.service.create'));
    app.post('/service/branch/create', passport.authenticate('authenticated', {session: false}), require('./service/controller/car.service.branch.create'));
    app.post('/service/branch/service/crate', passport.authenticate('authenticated', {session: false}), require('./service/controller/car.service.branch.service.create'));
};
