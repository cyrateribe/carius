const Joi = require('joi');
const Promise = require('bluebird');
const validate = Promise.promisify(Joi.validate);

const db = require(__base + 'core/service/db');
const schema = require('../const/vehicle.schema');

function create(params) {
    return validate(params, schema, {stripUnknown: true})
        .then((vehicle) => {
            return db.insert('cr_vehicles', vehicle);
        })
        .then((vehicle) => {
            return vehicle;
        })
}

module.exports = create;
