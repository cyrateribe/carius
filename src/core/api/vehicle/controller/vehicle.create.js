const Vehicle = require('../model/vehicle');


module.exports = (req, res, next) => {
    return Vehicle.create(req.body)
        .then((vehicle) => {
            res.json(vehicle);
        })
        .catch(next);
};

