const Joi = require('joi');
var schema = Joi.object().keys({
    vh_mf_id: Joi.number().min(1).required(),
    vh_mod_id: Joi.number().min(1),
    vh_body_id: Joi.number().min(1),
    vh_title: Joi.string().min(2),
    vh_mileage: Joi.number().greater(0),
    vh_vin: Joi.string().length(17),
    vh_production_year: Joi.number().integer().min(1500).required(),
    vh_fuel_type_id: Joi.number().min(1).required()
});
module.exports = schema;
