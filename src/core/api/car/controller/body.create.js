const Car = require('../model/car');

module.exports = (req, res, next) => {
    return Car.createBody(req.body)
        .then((carBody) => {
            res.json(carBody);
        })
        .catch(next);
};
