const Vehicle = require('../model/car');


module.exports = (req, res, next) => {
    return Vehicle.createManufacturer(req.body)
        .then((manufacturer) => {
            res.json(manufacturer);
        })
        .catch(next);
};

