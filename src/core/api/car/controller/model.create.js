const Car = require('../model/car');

module.exports = (req, res, next) => {
    return Car.createModel(req.body)
        .then((carModel) => {
            res.json(carModel);
        })
        .catch(next);
};
