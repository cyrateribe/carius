const Joi = require('joi');

var bodySchema = Joi.object().keys({
    body_name: Joi.string().min(1).required(),
    body_prod_year_from: Joi.number().integer().min(1500).required(),
    body_prod_year_to: Joi.number().integer().min(1500),
    body_mf_id: Joi.number().integer().min(1).required(),
    body_mod_id: Joi.number().min(1).required()
});

module.exports = bodySchema;
