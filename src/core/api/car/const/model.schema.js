const Joi = require('joi');

var modelSchema = Joi.object().keys({
    mod_name: Joi.string().min(1).required(),
    mod_prod_year_from: Joi.number().integer().min(1500).required(),
    mod_prod_year_to: Joi.number().integer().min(1500),
    mod_mf_id: Joi.number().integer().min(1).required()
});

module.exports = modelSchema;
