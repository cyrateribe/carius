const Joi = require('joi');

var schema = Joi.object().keys({
    mf_name: Joi.string().min(2).required(),
});

module.exports = schema;
