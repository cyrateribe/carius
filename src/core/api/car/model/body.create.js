const Joi = require('joi');
const Promise = require('bluebird');
const validate = Promise.promisify(Joi.validate);

const db = require(__base + 'core/service/db');
const bodySchema = require('../const/body.schema');

function createBody(params) {
    return validate(params, bodySchema, {stripUnknown: true})
        .then((body) => {
            return db.insert('cr_cars_bodies', body);
        })
        .then((body) => {
            return body;
        });
}

module.exports = createBody;
