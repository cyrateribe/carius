const Joi = require('joi');
const Promise = require('bluebird');
const validate = Promise.promisify(Joi.validate);

const db = require(__base + 'core/service/db');
const schema = require('../const/manufacturer.schema');

function createManufacturer(params) {
    return validate(params, schema, {stripUnknown: true})
        .then((manufacturer) => {
            return db.insert('cr_cars_manufacturers', manufacturer);
        })
        .then((manufacturer) => {
            return manufacturer;
        });
}

module.exports = createManufacturer;
