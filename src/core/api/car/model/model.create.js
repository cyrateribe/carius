const Joi = require('joi');
const Promise = require('bluebird');
const validate = Promise.promisify(Joi.validate);

const db = require(__base + 'core/service/db');
const modelSchema = require('../const/model.schema');

function createModel(params) {
    return validate(params, modelSchema, {stripUnknown: true})
        .then((model) => {
            return db.insert('cr_cars_models', model);
        })
        .then((model) => {
            return model;
        });
}

module.exports = createModel;
