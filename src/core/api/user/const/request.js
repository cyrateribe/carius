module.exports = {
    getUserByEmail: `SELECT * FROM cr_user WHERE lower(email) = lower($1) AND active = true;`,
    getUserById: `SELECT * FROM cr_user WHERE id = $1;`
};