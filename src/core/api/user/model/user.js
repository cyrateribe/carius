module.exports.activate = require('./user.activate');
module.exports.create = require('./user.create');
module.exports.getById = require('./user.getById');
module.exports.login = require('./user.login');
module.exports.update = require('./user.update');
