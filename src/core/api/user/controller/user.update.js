const User = require('../model/user');

module.exports = (req, res, next) => {
    return User.update(req.user.id, req.body)
        .then((user) => {
            res.json(user);
        })
        .catch(next);
};
