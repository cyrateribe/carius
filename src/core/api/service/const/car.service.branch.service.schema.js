const Joi = require('joi');

var schema = Joi.object().keys({
    bs_fk_ser_branch_id: Joi.number().min(1).required(),
    bs_fk_basic_service_id: Joi.number().min(1).required(),
    bs_price: Joi.number().min(1).required(),
    bs_desc: Joi.string().min(1)
});

module.exports = schema;