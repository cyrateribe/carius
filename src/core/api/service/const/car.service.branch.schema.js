const Joi = require('joi');

var schema = Joi.object().keys({
    sb_name: Joi.string().min(1).required(),
    sb_street_house: Joi.string().min(1).required(),
    sb_postal_code: Joi.number().required(),
    sb_phone: Joi.number().required(),
    sb_email: Joi.string().email().required(),
    sb_fk_service_id: Joi.string().required(),
    sb_latitude: Joi.number().required(),
    sb_longtitude: Joi.number().required()
});

module.exports = schema;