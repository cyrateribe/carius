const Joi = require('joi');

var schema = Joi.object().keys({
    cs_name: Joi.string().min(1).required(),
    cs_fk_main_branch: Joi.number(),
    cs_phone: Joi.number(),
    cs_email: Joi.string().email(),
    cs_description: Joi.string().min(1)
});

module.exports = schema;