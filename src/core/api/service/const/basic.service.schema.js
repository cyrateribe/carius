const Joi = require('joi');

var schema = Joi.object().keys({
    bs_desc: Joi.string().min(1).required()
});

module.exports = schema;