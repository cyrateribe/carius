const createBranch = require('../model/car.service.branch.create');

module.exports = (req, res, next) => {
    return createBranch(req.body)
        .then((branch) => {
            res.json(branch);
        })
        .catch(next);
}