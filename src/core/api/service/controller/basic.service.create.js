const BasicService = require('../model/basic.service.create');

module.exports = (req, res, next) => {
    return BasicService(req.body)
        .then(basicService => {
            res.json(basicService);
        })
        .catch(next);
}
