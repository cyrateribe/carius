const createCarService = require('../model/car.service.create');

module.exports = (req, res, next) => {
    return createCarService(req.body)
        .then((carService) => {
            res.json(carService);
        })
        .catch(next);
}
