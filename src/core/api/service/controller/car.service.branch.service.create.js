const branchService = require('../model/car.service.branch.service.create');

module.exports = (req, res, next) => {
    return branchService(req.body)
        .then(branchService => {
            res.json(branchService);
        })
        .catch(next);
}
