const Joi = require('joi');
const Promise = require('bluebird');
const validate = Promise.promisify(Joi.validate);

const db = require(__base + 'core/service/db');
const schema = require('../const/basic.service.schema');

function create(params) {
    return validate(params, schema, {stripUnknown: true})
        .then((basicService) => {
            return db.insert('cr_basic_services', basicService);
        })
        .then((basicService) => {
            return basicService;
        });
}

module.exports = create;