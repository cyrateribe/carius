const Joi = require('joi');
const Promise = require('bluebird');
const validate = Promise.promisify(Joi.validate);

const db = require(__base + 'core/service/db');
const schema = require('../const/car.service.branch.service.schema');

function create (params) {
    return validate(params, schema, {stripUnknown: true})
        .then((branchService) => {
            return db.insert('cr_branch_services', branchService);
        })
        .then((branchService) => {
            return branchService;
        });
}

module.exports = create;