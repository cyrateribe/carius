const Joi = require('joi');
const Promise = require('bluebird');
const validate = Promise.promisify(Joi.validate);

const db = require(__base + 'core/service/db');
const schema = require('../const/car.service.branch.schema');

function create(params) {
    return validate(params, schema, {stripUnknown: true})
        .then((serviceBranch) => {
            return db.insert('cr_services_branches', serviceBranch);
        })
        .then((serviceBranch) => {
            return serviceBranch;
        });
}

module.exports = create;