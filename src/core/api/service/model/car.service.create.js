const Joi = require('joi');
const Promise = require('bluebird');
const validate = Promise.promisify(Joi.validate);

const db = require(__base + 'core/service/db');
const schema = require('../const/car.service.schema');

function create(params) {
    return validate(params, schema, {stripUnknown: true})
        .then((carService) => {
            return db.insert('cr_car_services', carService);
        })
        .then((carService) => {
            return carService;
        });
}

module.exports = create;
