let redis = require('redis');
const Promise = require('bluebird');

Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
const config = require(__base + 'config.js');

let client = redis.createClient(config.redis);

module.exports = client;