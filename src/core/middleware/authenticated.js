const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const config = require(__base + 'config.js');
const User = require('../api/user/model/user');

const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeader(),
    secretOrKey: config.accessTokenJwt.secret
};


module.exports = function (passport) {
    passport.use('authenticated', new JwtStrategy(opts, function (jwt_payload, done) {
        User.getById(jwt_payload.id)
            .then((user) => {
                done(null, user);
            })
            .catch((err) => {
                done(err, false);
            });
    }));
};
