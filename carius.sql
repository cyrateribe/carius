PGDMP         )        	        v        	   carius_db    10.4    10.4 "    X           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            Y           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            Z           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            [           1262    16393 	   carius_db    DATABASE     g   CREATE DATABASE carius_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';
    DROP DATABASE carius_db;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            \           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    4                        3079    13241    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            ]           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1                        3079    16402 	   uuid-ossp 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
    DROP EXTENSION "uuid-ossp";
                  false    4            ^           0    0    EXTENSION "uuid-ossp"    COMMENT     W   COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';
                       false    2            �            1259    16438    cr_cars_bodies    TABLE     �   CREATE TABLE public.cr_cars_bodies (
    body_id bigint NOT NULL,
    body_name character varying NOT NULL,
    body_prod_year_from smallint NOT NULL,
    body_prod_year_to smallint,
    body_mf_id smallint NOT NULL,
    body_mod_id bigint NOT NULL
);
 "   DROP TABLE public.cr_cars_bodies;
       public         postgres    false    4            �            1259    16436    cr_cars_bodies_body_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cr_cars_bodies_body_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.cr_cars_bodies_body_id_seq;
       public       postgres    false    203    4            _           0    0    cr_cars_bodies_body_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.cr_cars_bodies_body_id_seq OWNED BY public.cr_cars_bodies.body_id;
            public       postgres    false    202            �            1259    16416    cr_cars_manufacturers    TABLE     r   CREATE TABLE public.cr_cars_manufacturers (
    mf_id integer NOT NULL,
    mf_name character varying NOT NULL
);
 )   DROP TABLE public.cr_cars_manufacturers;
       public         postgres    false    4            �            1259    16414    cr_cars_manufacturers_mf_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cr_cars_manufacturers_mf_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.cr_cars_manufacturers_mf_id_seq;
       public       postgres    false    4    199            `           0    0    cr_cars_manufacturers_mf_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.cr_cars_manufacturers_mf_id_seq OWNED BY public.cr_cars_manufacturers.mf_id;
            public       postgres    false    198            �            1259    16427    cr_cars_models    TABLE     �   CREATE TABLE public.cr_cars_models (
    mod_id bigint NOT NULL,
    mod_name character varying NOT NULL,
    mod_prod_year_from smallint NOT NULL,
    mod_prod_year_to smallint,
    mod_mf_id smallint NOT NULL
);
 "   DROP TABLE public.cr_cars_models;
       public         postgres    false    4            �            1259    16425    cr_cars_models_mod_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cr_cars_models_mod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.cr_cars_models_mod_id_seq;
       public       postgres    false    4    201            a           0    0    cr_cars_models_mod_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.cr_cars_models_mod_id_seq OWNED BY public.cr_cars_models.mod_id;
            public       postgres    false    200            �            1259    16394    cr_user    TABLE     �  CREATE TABLE public.cr_user (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name character varying NOT NULL,
    email character varying NOT NULL,
    password character varying NOT NULL,
    active boolean,
    last_connected timestamp with time zone,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    is_deleted boolean,
    language character varying
);
    DROP TABLE public.cr_user;
       public         postgres    false    2    4    4            �           2604    16441    cr_cars_bodies body_id    DEFAULT     �   ALTER TABLE ONLY public.cr_cars_bodies ALTER COLUMN body_id SET DEFAULT nextval('public.cr_cars_bodies_body_id_seq'::regclass);
 E   ALTER TABLE public.cr_cars_bodies ALTER COLUMN body_id DROP DEFAULT;
       public       postgres    false    202    203    203            �           2604    16419    cr_cars_manufacturers mf_id    DEFAULT     �   ALTER TABLE ONLY public.cr_cars_manufacturers ALTER COLUMN mf_id SET DEFAULT nextval('public.cr_cars_manufacturers_mf_id_seq'::regclass);
 J   ALTER TABLE public.cr_cars_manufacturers ALTER COLUMN mf_id DROP DEFAULT;
       public       postgres    false    198    199    199            �           2604    16430    cr_cars_models mod_id    DEFAULT     ~   ALTER TABLE ONLY public.cr_cars_models ALTER COLUMN mod_id SET DEFAULT nextval('public.cr_cars_models_mod_id_seq'::regclass);
 D   ALTER TABLE public.cr_cars_models ALTER COLUMN mod_id DROP DEFAULT;
       public       postgres    false    201    200    201            U          0    16438    cr_cars_bodies 
   TABLE DATA               }   COPY public.cr_cars_bodies (body_id, body_name, body_prod_year_from, body_prod_year_to, body_mf_id, body_mod_id) FROM stdin;
    public       postgres    false    203   H%       Q          0    16416    cr_cars_manufacturers 
   TABLE DATA               ?   COPY public.cr_cars_manufacturers (mf_id, mf_name) FROM stdin;
    public       postgres    false    199   u%       S          0    16427    cr_cars_models 
   TABLE DATA               k   COPY public.cr_cars_models (mod_id, mod_name, mod_prod_year_from, mod_prod_year_to, mod_mf_id) FROM stdin;
    public       postgres    false    201   �%       O          0    16394    cr_user 
   TABLE DATA               �   COPY public.cr_user (id, name, email, password, active, last_connected, created_at, updated_at, is_deleted, language) FROM stdin;
    public       postgres    false    197   �%       b           0    0    cr_cars_bodies_body_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.cr_cars_bodies_body_id_seq', 1, true);
            public       postgres    false    202            c           0    0    cr_cars_manufacturers_mf_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.cr_cars_manufacturers_mf_id_seq', 1, true);
            public       postgres    false    198            d           0    0    cr_cars_models_mod_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.cr_cars_models_mod_id_seq', 1, true);
            public       postgres    false    200            �           2606    16446 "   cr_cars_bodies cr_cars_bodies_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.cr_cars_bodies
    ADD CONSTRAINT cr_cars_bodies_pkey PRIMARY KEY (body_id);
 L   ALTER TABLE ONLY public.cr_cars_bodies DROP CONSTRAINT cr_cars_bodies_pkey;
       public         postgres    false    203            �           2606    16424 0   cr_cars_manufacturers cr_cars_manufacturers_pkey 
   CONSTRAINT     q   ALTER TABLE ONLY public.cr_cars_manufacturers
    ADD CONSTRAINT cr_cars_manufacturers_pkey PRIMARY KEY (mf_id);
 Z   ALTER TABLE ONLY public.cr_cars_manufacturers DROP CONSTRAINT cr_cars_manufacturers_pkey;
       public         postgres    false    199            �           2606    16435 "   cr_cars_models cr_cars_models_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.cr_cars_models
    ADD CONSTRAINT cr_cars_models_pkey PRIMARY KEY (mod_id);
 L   ALTER TABLE ONLY public.cr_cars_models DROP CONSTRAINT cr_cars_models_pkey;
       public         postgres    false    201            �           2606    16401    cr_user cr_user_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.cr_user
    ADD CONSTRAINT cr_user_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.cr_user DROP CONSTRAINT cr_user_pkey;
       public         postgres    false    197            U      x�3�t�4200�Ɯ���\1z\\\ )�C      Q      x�3��/H������ �}      S       x�3�KM.)J�4��4���4����� K;       O   �   x�E���   �3<E� ?�x�e]l���yAт4�a���c߾t`��m������#AA�0�R�Rp�}*�����w����n����h�wv�%�-�5�b~l�q�_���CR�:-�ڤ�_�Y���{[�p�W�����a�!�?Ђ.�     